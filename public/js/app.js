(function($, document) {

    $(document).ready(function() {
        // Mobile menu toggle 
        $(".menu-toggle").click(function() {
            $(".mobile-navigation").slideToggle();
        });
    });
})(jQuery, document);
import Footer from "./Components/Footer";
import Header from "./Components/Header";
import ForecastDashboard from "./Components/ForecastDashboard";

function App() {
  return (
    <div className="site-content">
        <Header/>

        <ForecastDashboard/>

        <Footer/>
    </div>
  );
}

export default App;

import React from "react";

class Footer extends React.Component {
    render() {
        return (
            <footer className="site-footer">
                <div className="container">
                    <p className="colophon">Copyright 2019-20 Study Link.</p>
                </div>
            </footer>
        );
    }
}

export default Footer;
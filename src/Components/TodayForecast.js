import React from "react";

class TodayForecast extends React.Component {
    render() {
        return (
            <div className="today forecast">
                <div className="forecast-header">
                    <div className="day">{this.props.day}</div>
                    <div className="date">{this.props.date}</div>
                </div>
                <div className="forecast-content">
                    <div className="location">{this.props.cityName}</div>
                    <div className="degree">
                        <div className="num temp">{this.props.temp}<sup>o</sup>C</div>
                        <div className="forecast-icon">
                            <img className="weather-icon" src={this.props.icon} alt="weather icon" width='90'></img>
                        </div>
                    </div>
                    <span><img src="/images/icon-umberella.png" alt=""></img><span id="humidity">{this.props.humidity}%</span></span>
                    <span><img src="/images/icon-wind.png" alt=""></img><span id="wind-speed">{this.props.windSpeed}km/h</span></span>
                    <span><img src="/images/icon-compass.png" alt=""></img><span id="wind-degree">{this.props.windDegree}<sup>o</sup></span></span>
                </div>
            </div>
        );
    }
}

export default TodayForecast;


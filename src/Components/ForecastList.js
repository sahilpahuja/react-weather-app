import React from "react";
import TodayForecast from "./TodayForecast";
import OtherDayForecast from "./OtherDayForecast";

class ForecastList extends React.Component {
    render() {
        const otherDayForecasts = this.props.forecasts.slice(1).map((forecast) => (
            <OtherDayForecast
                key={forecast.id}
                day={forecast.dayName}
                temp={forecast.temp}
                icon={forecast.icon}
            />
        ));
        return (
            <div className="forecast-container">
                <TodayForecast
                    cityName={this.props.cityName}
                    humidity={this.props.todaysExtraDetails.humidity}
                    windSpeed={this.props.todaysExtraDetails.windSpeed}
                    windDegree={this.props.todaysExtraDetails.windDegree}
                    date={this.props.todaysExtraDetails.date}
                    day={this.props.cityName}
                    temp={this.props.forecasts.length === 0 ? '' : this.props.forecasts[0].temp}
                    icon={this.props.forecasts.length === 0 ? '' :this.props.forecasts[0].icon}
                />
                {otherDayForecasts}
            </div>
        );
    }
}

export default ForecastList;
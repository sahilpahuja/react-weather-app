import React from "react";

class Header extends React.Component {
    render() {
        return (
            <div className="site-header">
                <div className="container">
                    <a href="/index.html" className="branding">
                        <img src="/images/logo.png" alt="" className="logo"></img>
                        <div className="logo-type">
                            <h1 className="site-title">Clima</h1>
                            <small className="site-description">A future forecast</small>
                        </div>

                    </a>

                    <div className="main-navigation">
                        <button type="button" className="menu-toggle"><i className="fa fa-bars"></i></button>
                        <ul className="menu">
                            <li className="menu-item current-menu-item"><a href="/index.html">Home</a></li>
                        </ul>
                    </div>

                    <div className="mobile-navigation">
                        <ul className="menu">
                            <li className="menu-item current-menu-item"><a href="/index.html">Home</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default Header;
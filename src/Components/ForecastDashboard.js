import React from "react";
import Search from "./Search";
import ForecastList from "./ForecastList";
import Client from "../Client";

class ForecastDashboard extends React.Component {
    state = {
        forecasts: {
            cityName: "mumbai",
            todaysExtraDetails: {
                humidity: '',
                windSpeed: '',
                windDegree: '',
            },
            forecastsDetails: [],
        }
    };

    componentDidMount() {
        this.loadForecastFromServer("mumbai");
    }

    loadForecastFromServer = (cityName) => {
        Client.getForecast(cityName, serverResponse => {
            this.updateData(serverResponse, cityName);
        });
    }

    onSearch = (cityName) => {
        if(cityName === ""){
            cityName = Client.defaultCity;
        }
        this.loadForecastFromServer(cityName);
    }

    updateData = (data, cityName) => {
        const imagePath = "images/icons/";

        let extraDetails = {};

        // humidity
        extraDetails.humidity = data.list[0].main.humidity;

        // wind speed
        //1 m/sec = 3.6km/hr
        extraDetails.windSpeed = Math.round((data.list[0].wind.speed * 3.6 * 10))/10;

        //wind Degree
        extraDetails.windDegree = data.list[0].wind.deg;

        const todaysDate = new Date(data.list[0].dt_txt);
        const todaysMonth = Client.monthsName[todaysDate.getMonth()];
        const todaysDay = todaysDate.getDay();
        extraDetails.date = todaysDate.getDate() + " " + todaysMonth;

        let forecasts = [];
        var j=0;
        for(let i=0; i<5; ++i){
            let forecast = {};
            forecast.id = data.list[j].dt;
            forecast.dayName = Client.daysName[(todaysDay + i) %7];
            forecast.temp = Math.round(data.list[j].main.temp *10)/10;// "<sup>o</sup>C"
            forecast.icon = imagePath + data.list[j].weather[0].icon + ".svg";
            forecasts.push(forecast);
            j+=8;
        }
        this.setState({forecasts: {
            cityName: cityName,
            todaysExtraDetails: extraDetails,
            forecastsDetails: forecasts,
        }});
    }

    render() {
        return (
            <>
                <Search
                    onSearch={this.onSearch}
                />

                <div className="forecast-table">
                    <div className="container">
                        <ForecastList
                            cityName={this.state.forecasts.cityName}
                            todaysExtraDetails={this.state.forecasts.todaysExtraDetails}
                            forecasts={this.state.forecasts.forecastsDetails}
                        />
                    </div>
                </div>
            </>
        );
    }
}

export default ForecastDashboard;
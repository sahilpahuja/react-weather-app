import React from "react";
import ForecastList from "./ForecastList";

class Forecast extends React.Component {
    render() {
        return (
            <ForecastList />
        );
    }
}

export default Forecast;
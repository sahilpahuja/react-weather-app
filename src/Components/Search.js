import React from "react";

class Search extends React.Component {
    state = {
        fields: {
            city: '',
        },
    };

    onInputChange = evt => {
        const fields = {
            city: evt.target.value
        };

        this.setState({fields});
    }

    onFormSubmit = (evt) => {
        evt.preventDefault();

        const city = this.state.fields.city;
        this.props.onSearch(city);
    }

    render() {
        return (
            <div className="hero">
                <div className="container">
                    <form onSubmit={this.onFormSubmit} className="find-location">
                        <input type="text"
                               placeholder="Find your location..."
                               name="city"
                               id="city"
                               value={this.state.fields.city}
                               onChange={this.onInputChange}
                        />
                        <input type="submit" value="Find"/>
                    </form>
                </div>
            </div>
        );
    }
}

export default Search;
import React from "react";

class OtherDayForecast extends React.Component {
    render() {
        return (
            <div className="forecast">
                <div className="forecast-header">
                    <div className="day">{this.props.day}</div>
                </div>
                <div className="forecast-content">
                    <div className="forecast-icon">
                        <img className="weather-icon" src={this.props.icon} alt="weather icon" width='48'></img>
                    </div>
                    <div className="degree temp">{this.props.temp}<sup>o</sup>C</div>
                </div>
            </div>
        );
    }
}

export default OtherDayForecast;


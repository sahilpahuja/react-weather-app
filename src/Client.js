const Client = (function() {
    const key = "81304f9283b938b8f2ccae61f3034d5c";
    const defaultCity = "Mumbai";
    const daysName = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    const monthsName = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

    function getForecast(city, success, error=null) {
        const base = "http://api.openweathermap.org/data/2.5/forecast";
        const query = `?q=${city}&units=metric&APPID=${key}`;

        fetch(base+query, {
            headers: {
                Accept: 'application/json',
            },
        }).then(checkStatus)
            .then(parseJSON)
            .then(success);
    }

    function checkStatus(response) {
        if (response.status >= 200 && response.status < 300) {
            return response;
        } else {
            const error = new Error(`HTTP Error ${response.statusText}`);
            error.status = response.status;
            error.response = response;
            console.log(error);
            throw error;
        }
    }
    function parseJSON(response) {
        return response.json();
    }

    return {
        defaultCity,
        daysName,
        monthsName,
        getForecast,
    };
}());

export default Client;